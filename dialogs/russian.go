package dialogs

import "fmt"

type RussianHangmanDialogs struct{}

func NewRussianHangmanDialogs() *RussianHangmanDialogs {
	return &RussianHangmanDialogs{}
}

func (d *RussianHangmanDialogs) CharGuessPrompt() string {
	return "> Угадай букву:\n< "
}

func (d *RussianHangmanDialogs) WrongInput() string {
	return "> Введи одну букву\n"
}

func (d *RussianHangmanDialogs) Mistake(currentMistakeNumber uint,
	totalAllowedMistakes uint) string {
	return fmt.Sprintf("> Не угадал! Ошибка %d из %d\n", currentMistakeNumber, totalAllowedMistakes)
}

func (d *RussianHangmanDialogs) Hit() string {
	return "> Угадал!\n"
}

func (d *RussianHangmanDialogs) Repeat() string {
	return "> Ты уже открыл эту букву!\n"
}

func (d *RussianHangmanDialogs) CurrentGuessedWord(currentGuessedWord []rune) string {
	return "" +
		">\n" +
		fmt.Sprintf("> Слово: %s\n", makeGuessedWordFromStatus(currentGuessedWord, "*")) +
		">\n"
}

func (d *RussianHangmanDialogs) Lost() string {
	return "> Ты проиграл!\n"
}

func (d *RussianHangmanDialogs) Win() string {
	return "> Ты выиграл!\n"
}
