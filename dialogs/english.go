package dialogs

import "fmt"

type EnglishHangmanDialogs struct{}

func NewEnglishHangmanDialogs() *EnglishHangmanDialogs {
	return &EnglishHangmanDialogs{}
}

func (d *EnglishHangmanDialogs) CharGuessPrompt() string {
	return "> Guess a letter:\n< "
}

func (d *EnglishHangmanDialogs) WrongInput() string {
	return "> Expect one character input\n"
}

func (d *EnglishHangmanDialogs) Mistake(currentMistakeNumber uint,
	totalAllowedMistakes uint) string {
	return fmt.Sprintf("> Missed, mistake %d out of %d\n", currentMistakeNumber, totalAllowedMistakes)
}

func (d *EnglishHangmanDialogs) Hit() string {
	return "> Hit!\n"
}

func (d *EnglishHangmanDialogs) Repeat() string {
	return "> You already guessed this letter!\n"
}

func (d *EnglishHangmanDialogs) CurrentGuessedWord(currentGuessedWord []rune) string {
	return "" +
		">\n" +
		fmt.Sprintf("> The word: %s\n", makeGuessedWordFromStatus(currentGuessedWord, "*")) +
		">\n"
}

func (d *EnglishHangmanDialogs) Lost() string {
	return "> You lost!\n"
}

func (d *EnglishHangmanDialogs) Win() string {
	return "> You won!\n"
}
