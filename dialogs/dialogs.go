package dialogs

import (
	"gitlab.com/tmilovanov/hangman/communicator"
)

type DialogLanguage string

const (
	DialogLanguageEnglish DialogLanguage = "english"
	DialogLanguageRussian DialogLanguage = "russian"
)

func NewDialog(language DialogLanguage) communicator.HangmanDialog {
	switch language {
	case DialogLanguageRussian:
		return NewRussianHangmanDialogs()
	case DialogLanguageEnglish:
		return NewEnglishHangmanDialogs()
	default:
		return NewEnglishHangmanDialogs()
	}
}

func makeGuessedWordFromStatus(guessedStatus []rune, charForNotGuessed string) string {
	result := ""
	for _, chr := range guessedStatus {
		if chr == 0 {
			result += charForNotGuessed
		} else {
			result += string(chr)
		}
	}

	return result
}
