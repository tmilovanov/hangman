package dictionary

import (
	"math/rand"
	"time"
)

type SimpleRandomDictionary struct {
	words         []string
	randGenerator *rand.Rand
}

func NewSimpleRandomDistionary(words []string) *SimpleRandomDictionary {
	return &SimpleRandomDictionary{
		words:         words,
		randGenerator: rand.New(rand.NewSource(time.Now().UnixNano())),
	}
}

func (d *SimpleRandomDictionary) GenerateWord() string {
	randomIndex := d.randGenerator.Intn(len(d.words))
	return d.words[randomIndex]
}
