package hangman_test

import (
	"errors"
	"testing"

	"gitlab.com/tmilovanov/hangman/hangman"

	"github.com/stretchr/testify/require"
)

func Test_HangmanGameLoop_StopOnWinOrLost(t *testing.T) {
	context := hangman.NewGameContext("test", 3)
	err := hangman.RunHangmanGameLoop(context, newFailableCommunicator([]rune("tes"), -1))
	require.Nil(t, err)
	require.Equal(t, hangman.GameStatusWin, context.GameStatus())
}

func Test_HangmanGameLoop_StopOnFetchError(t *testing.T) {
	context := hangman.NewGameContext("test", 3)
	err := hangman.RunHangmanGameLoop(context, newFailableCommunicator([]rune{'t', 'e', 0}, -1))
	require.Equal(t, errTestFailedToFetch, err)
	require.Equal(t, []rune{'t', 'e', 0, 't'}, context.CurrentlyGuessed)
}

func Test_HangmanGameLoop_StopOnDisplayError(t *testing.T) {
	context := hangman.NewGameContext("test", 3)
	err := hangman.RunHangmanGameLoop(context, newFailableCommunicator([]rune{'t', 'e', 's', 't'}, 2))
	require.Equal(t, errTestFailedToDisplay, err)
	require.Equal(t, []rune{'t', 'e', 0, 't'}, context.CurrentlyGuessed)
}

var errTestFailedToFetch = errors.New("test error - failed to fetch player guess")
var errTestFailedToDisplay = errors.New("test error - failed to display game status")

type failableCommunicator struct {
	symbolsToFeed   []rune
	currentSymbolI  int
	failToDisplayOn int
}

func newFailableCommunicator(symbolsToFeed []rune, failToDisplayOn int) *failableCommunicator {
	return &failableCommunicator{
		symbolsToFeed:   symbolsToFeed,
		currentSymbolI:  0,
		failToDisplayOn: failToDisplayOn,
	}
}

func (c *failableCommunicator) FetchPlayerGuess() (rune, error) {
	result := c.symbolsToFeed[c.currentSymbolI]
	if result == 0 {
		return 0, errTestFailedToFetch
	}
	c.currentSymbolI++
	return result, nil
}

func (c *failableCommunicator) DisplayRoundResult(guessResult hangman.GuessResult, gameContext *hangman.GameContext) error {
	if c.failToDisplayOn == -1 {
		return nil
	}

	if c.currentSymbolI == c.failToDisplayOn {
		return errTestFailedToDisplay
	}

	return nil
}
