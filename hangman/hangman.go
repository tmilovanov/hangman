package hangman

type (
	GameContext struct {
		charsStatus map[rune]*charStatus
		leftToGuess uint

		MistakesDone         uint
		TotalAllowedMistakes uint
		CurrentlyGuessed     []rune
	}

	GameStatus  int
	GuessResult int
)

const (
	GameStatusRunning GameStatus = iota
	GameStatusWin
	GameStatusLost
)

const (
	GuessResultMistake GuessResult = iota
	GuessResultHit
	GuessResultRepeat
)

// Internal types.
type (
	charStatus struct {
		IsGuessed     bool
		IndexesInWord []uint
	}
)

func NewGameContext(word string, totalAllowedMistakes uint) *GameContext {
	wordRunes := []rune(word)
	return &GameContext{
		charsStatus:          fillInCharsStatus(wordRunes),
		leftToGuess:          uint(len(wordRunes)),
		MistakesDone:         0,
		TotalAllowedMistakes: totalAllowedMistakes,
		CurrentlyGuessed:     make([]rune, len(wordRunes)),
	}
}

func (c *GameContext) MakeGuess(playerGuess rune) GuessResult {
	chrStatus, isCharExist := c.charsStatus[playerGuess]
	if !isCharExist {
		c.MistakesDone++

		return GuessResultMistake
	} else {
		if chrStatus.IsGuessed {
			return GuessResultRepeat
		}

		chrStatus.IsGuessed = true

		for _, index := range chrStatus.IndexesInWord {
			c.CurrentlyGuessed[index] = playerGuess
			c.leftToGuess--
		}

		return GuessResultHit
	}
}

func (c *GameContext) GameStatus() GameStatus {
	if c.MistakesDone > 0 && c.MistakesDone >= c.TotalAllowedMistakes {
		return GameStatusLost
	}

	if c.leftToGuess <= 0 {
		return GameStatusWin
	}

	return GameStatusRunning
}

func fillInCharsStatus(word []rune) map[rune]*charStatus {
	charsStatus := make(map[rune]*charStatus)
	for chrIndex, char := range word {
		chrStatus, exist := charsStatus[char]
		if !exist {
			chrStatus = &charStatus{IsGuessed: false}
			charsStatus[char] = chrStatus
		}

		chrStatus.IndexesInWord = append(chrStatus.IndexesInWord, uint(chrIndex))
	}
	return charsStatus
}
