package hangman

type (
	PlayerCommunicator interface {
		FetchPlayerGuess() (rune, error)
		DisplayRoundResult(guessResult GuessResult, gameContext *GameContext) error
	}
)

func RunHangmanGameLoop(gameContext *GameContext, playerCommunicator PlayerCommunicator) error {
	for gameContext.GameStatus() == GameStatusRunning {
		playerGuess, err := playerCommunicator.FetchPlayerGuess()
		if err != nil {
			return err
		}

		guessResult := gameContext.MakeGuess(playerGuess)

		err = playerCommunicator.DisplayRoundResult(guessResult, gameContext)
		if err != nil {
			return err
		}
	}

	return nil
}
