package hangman_test

import (
	"testing"

	"gitlab.com/tmilovanov/hangman/hangman"

	"github.com/stretchr/testify/require"
)

// Проигрываем.
func Test_GameContext_Lost(t *testing.T) {
	c := hangman.NewGameContext("test", 3)

	var guessResult hangman.GuessResult

	guessResult = c.MakeGuess('t')
	require.Equal(t, hangman.GuessResultHit, guessResult)
	require.Equal(t, hangman.GameStatusRunning, c.GameStatus())
	require.Equal(t, []rune{'t', 0, 0, 't'}, c.CurrentlyGuessed)
	require.Equal(t, uint(0), c.MistakesDone)

	guessResult = c.MakeGuess('й')
	require.Equal(t, hangman.GuessResultMistake, guessResult)
	require.Equal(t, hangman.GameStatusRunning, c.GameStatus())
	require.Equal(t, []rune{'t', 0, 0, 't'}, c.CurrentlyGuessed)
	require.Equal(t, uint(1), c.MistakesDone)

	guessResult = c.MakeGuess('x')
	require.Equal(t, hangman.GuessResultMistake, guessResult)
	require.Equal(t, hangman.GameStatusRunning, c.GameStatus())
	require.Equal(t, []rune{'t', 0, 0, 't'}, c.CurrentlyGuessed)
	require.Equal(t, uint(2), c.MistakesDone)

	guessResult = c.MakeGuess('s')
	require.Equal(t, hangman.GuessResultHit, guessResult)
	require.Equal(t, hangman.GameStatusRunning, c.GameStatus())
	require.Equal(t, []rune{'t', 0, 's', 't'}, c.CurrentlyGuessed)
	require.Equal(t, uint(2), c.MistakesDone)

	guessResult = c.MakeGuess('s')
	require.Equal(t, hangman.GuessResultRepeat, guessResult)
	require.Equal(t, hangman.GameStatusRunning, c.GameStatus())
	require.Equal(t, []rune{'t', 0, 's', 't'}, c.CurrentlyGuessed)
	require.Equal(t, uint(2), c.MistakesDone)

	guessResult = c.MakeGuess('t')
	require.Equal(t, hangman.GuessResultRepeat, guessResult)
	require.Equal(t, hangman.GameStatusRunning, c.GameStatus())
	require.Equal(t, []rune{'t', 0, 's', 't'}, c.CurrentlyGuessed)
	require.Equal(t, uint(2), c.MistakesDone)

	guessResult = c.MakeGuess('x')
	require.Equal(t, hangman.GuessResultMistake, guessResult)
	require.Equal(t, hangman.GameStatusLost, c.GameStatus())
	require.Equal(t, []rune{'t', 0, 's', 't'}, c.CurrentlyGuessed)
}

// Выигрываем.
func Test_GameContext_Win(t *testing.T) {
	c := hangman.NewGameContext("test", 3)

	var guessResult hangman.GuessResult

	guessResult = c.MakeGuess('t')
	require.Equal(t, hangman.GuessResultHit, guessResult)
	require.Equal(t, hangman.GameStatusRunning, c.GameStatus())
	require.Equal(t, []rune{'t', 0, 0, 't'}, c.CurrentlyGuessed)
	require.Equal(t, uint(0), c.MistakesDone)

	guessResult = c.MakeGuess('й')
	require.Equal(t, hangman.GuessResultMistake, guessResult)
	require.Equal(t, hangman.GameStatusRunning, c.GameStatus())
	require.Equal(t, []rune{'t', 0, 0, 't'}, c.CurrentlyGuessed)
	require.Equal(t, uint(1), c.MistakesDone)

	guessResult = c.MakeGuess('x')
	require.Equal(t, hangman.GuessResultMistake, guessResult)
	require.Equal(t, hangman.GameStatusRunning, c.GameStatus())
	require.Equal(t, []rune{'t', 0, 0, 't'}, c.CurrentlyGuessed)
	require.Equal(t, uint(2), c.MistakesDone)

	guessResult = c.MakeGuess('s')
	require.Equal(t, hangman.GuessResultHit, guessResult)
	require.Equal(t, hangman.GameStatusRunning, c.GameStatus())
	require.Equal(t, []rune{'t', 0, 's', 't'}, c.CurrentlyGuessed)
	require.Equal(t, uint(2), c.MistakesDone)

	guessResult = c.MakeGuess('s')
	require.Equal(t, hangman.GuessResultRepeat, guessResult)
	require.Equal(t, hangman.GameStatusRunning, c.GameStatus())
	require.Equal(t, []rune{'t', 0, 's', 't'}, c.CurrentlyGuessed)
	require.Equal(t, uint(2), c.MistakesDone)

	guessResult = c.MakeGuess('t')
	require.Equal(t, hangman.GuessResultRepeat, guessResult)
	require.Equal(t, hangman.GameStatusRunning, c.GameStatus())
	require.Equal(t, []rune{'t', 0, 's', 't'}, c.CurrentlyGuessed)
	require.Equal(t, uint(2), c.MistakesDone)

	guessResult = c.MakeGuess('e')
	require.Equal(t, hangman.GuessResultHit, guessResult)
	require.Equal(t, hangman.GameStatusWin, c.GameStatus())
	require.Equal(t, []rune{'t', 'e', 's', 't'}, c.CurrentlyGuessed)
}

// Проигрываем в игру с 0 количеством позволенных ошибок.
func Test_GameContext_Godmode_Lost(t *testing.T) {
	c := hangman.NewGameContext("test", 0)

	guessResult := c.MakeGuess('a')
	require.Equal(t, hangman.GuessResultMistake, guessResult)
	require.Equal(t, hangman.GameStatusLost, c.GameStatus())
	require.Equal(t, []rune{0, 0, 0, 0}, c.CurrentlyGuessed)
}

// Выигрываем в игру с 0 количеством позволенных ошибок.
func Test_GameContext_Godmode_Win(t *testing.T) {
	c := hangman.NewGameContext("test", 0)

	var guessResult hangman.GuessResult

	guessResult = c.MakeGuess('t')
	require.Equal(t, hangman.GuessResultHit, guessResult)
	require.Equal(t, hangman.GameStatusRunning, c.GameStatus())
	require.Equal(t, []rune{'t', 0, 0, 't'}, c.CurrentlyGuessed)

	guessResult = c.MakeGuess('s')
	require.Equal(t, hangman.GuessResultHit, guessResult)
	require.Equal(t, hangman.GameStatusRunning, c.GameStatus())
	require.Equal(t, []rune{'t', 0, 's', 't'}, c.CurrentlyGuessed)

	guessResult = c.MakeGuess('e')
	require.Equal(t, hangman.GuessResultHit, guessResult)
	require.Equal(t, hangman.GameStatusWin, c.GameStatus())
	require.Equal(t, []rune{'t', 'e', 's', 't'}, c.CurrentlyGuessed)
}

// Русские символы вместо английских.
func Test_GameContext_NonAsciiWords(t *testing.T) {
	c := hangman.NewGameContext("привет", 5)

	var guessResult hangman.GuessResult

	guessResult = c.MakeGuess('в')
	require.Equal(t, hangman.GuessResultHit, guessResult)
	require.Equal(t, hangman.GameStatusRunning, c.GameStatus())
	require.Equal(t, []rune{0, 0, 0, 'в', 0, 0}, c.CurrentlyGuessed)

	guessResult = c.MakeGuess('х')
	require.Equal(t, hangman.GuessResultMistake, guessResult)
	require.Equal(t, hangman.GameStatusRunning, c.GameStatus())
	require.Equal(t, []rune{0, 0, 0, 'в', 0, 0}, c.CurrentlyGuessed)

	guessResult = c.MakeGuess('п')
	require.Equal(t, hangman.GuessResultHit, guessResult)
	require.Equal(t, hangman.GameStatusRunning, c.GameStatus())
	require.Equal(t, []rune{'п', 0, 0, 'в', 0, 0}, c.CurrentlyGuessed)

	guessResult = c.MakeGuess('и')
	require.Equal(t, hangman.GuessResultHit, guessResult)
	require.Equal(t, hangman.GameStatusRunning, c.GameStatus())
	require.Equal(t, []rune{'п', 0, 'и', 'в', 0, 0}, c.CurrentlyGuessed)

	guessResult = c.MakeGuess('р')
	require.Equal(t, hangman.GuessResultHit, guessResult)
	require.Equal(t, hangman.GameStatusRunning, c.GameStatus())
	require.Equal(t, []rune{'п', 'р', 'и', 'в', 0, 0}, c.CurrentlyGuessed)

	guessResult = c.MakeGuess('е')
	require.Equal(t, hangman.GuessResultHit, guessResult)
	require.Equal(t, hangman.GameStatusRunning, c.GameStatus())
	require.Equal(t, []rune{'п', 'р', 'и', 'в', 'е', 0}, c.CurrentlyGuessed)

	guessResult = c.MakeGuess('т')
	require.Equal(t, hangman.GuessResultHit, guessResult)
	require.Equal(t, hangman.GameStatusWin, c.GameStatus())
	require.Equal(t, []rune{'п', 'р', 'и', 'в', 'е', 'т'}, c.CurrentlyGuessed)
}
