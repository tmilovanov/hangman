package main

import (
	"fmt"

	"gitlab.com/tmilovanov/hangman/communicator"
	"gitlab.com/tmilovanov/hangman/dialogs"
	"gitlab.com/tmilovanov/hangman/dictionary"
	"gitlab.com/tmilovanov/hangman/hangman"

	"github.com/alecthomas/kong"
)

type CliGameConfig struct {
	dictionary           []string
	TotalAllowedMistakes uint                   `default:"5"`
	Language             dialogs.DialogLanguage `help:"Game language (\"english\" or \"russian\")" default:"english"`
}

func main() {
	gameConfig := CliGameConfig{
		dictionary: []string{"hello", "world", "hangman", "game"},
	}
	kong.Parse(&gameConfig)
	runGame(&gameConfig)
}

func runGame(gameConfig *CliGameConfig) {
	words := dictionary.NewSimpleRandomDistionary(gameConfig.dictionary)
	err := hangman.RunHangmanGameLoop(
		hangman.NewGameContext(words.GenerateWord(), gameConfig.TotalAllowedMistakes),
		communicator.NewConsoleCommunicator(
			dialogs.NewDialog(gameConfig.Language),
		),
	)
	if err != nil {
		fmt.Printf("failed to run game: %s\n", err)
	}
}
