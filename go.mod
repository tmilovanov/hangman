module gitlab.com/tmilovanov/hangman

go 1.16

require (
	github.com/alecthomas/kong v0.2.17
	github.com/stretchr/testify v1.7.0
)
