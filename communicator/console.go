package communicator

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	"gitlab.com/tmilovanov/hangman/hangman"
)

type HangmanDialog interface {
	CharGuessPrompt() string
	WrongInput() string
	Mistake(currentMistakeNumber uint, totalAllowedMistakes uint) string
	Hit() string
	Repeat() string
	CurrentGuessedWord(currentGuessedWord []rune) string
	Lost() string
	Win() string
}

type ConsoleCommunicator struct {
	reader *bufio.Reader
	dialog HangmanDialog
}

func NewConsoleCommunicator(dialog HangmanDialog) *ConsoleCommunicator {
	return &ConsoleCommunicator{
		reader: bufio.NewReader(os.Stdin),
		dialog: dialog,
	}
}

func (c *ConsoleCommunicator) FetchPlayerGuess() (rune, error) {
	for {
		fmt.Print(c.dialog.CharGuessPrompt())

		line, err := c.reader.ReadString('\n')
		if err != nil {
			return 0, err
		}

		chars := []rune(strings.TrimSpace(line))
		if len(chars) != 1 {
			fmt.Print(c.dialog.WrongInput())
		} else {
			return chars[0], nil
		}
	}
}

func (c *ConsoleCommunicator) DisplayRoundResult(guessResult hangman.GuessResult, gameContext *hangman.GameContext) error {
	switch guessResult {
	case hangman.GuessResultMistake:
		fmt.Print(c.dialog.Mistake(gameContext.MistakesDone,
			gameContext.TotalAllowedMistakes))
	case hangman.GuessResultHit:
		fmt.Print(c.dialog.Hit())
	case hangman.GuessResultRepeat:
		fmt.Print(c.dialog.Repeat())
	}

	switch gameContext.GameStatus() {
	case hangman.GameStatusLost:
		fmt.Print(c.dialog.Lost())
	case hangman.GameStatusWin:
		fmt.Print(c.dialog.Win())
	case hangman.GameStatusRunning:
		fmt.Print(c.dialog.CurrentGuessedWord(gameContext.CurrentlyGuessed))
	}

	return nil
}
