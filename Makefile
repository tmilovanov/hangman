GOPATH = $(shell go env GOPATH)
GOLANGCI_LINT_INSTALL_PATH = $(GOPATH)/bin
GOLANGCI_LINT_REQUIRED_VERSION = 1.40.1
GOLANGCI_LINT_CURRENT_VERSION = $(shell (which golangci-lint && golangci-lint --version) | tail -n 1 | awk '{print $$4}')

.PHONY: build test lint golangci-clint

build:
	go build -o hangman-game cmd/console_hangman/main.go

test:
	go test ./...

# Right now linter check only erros in new code
# But we need to fix errors in old code and later
# remove option --new-from-rev
lint: golangci-lint
	$(GOLANGCI_LINT) run ./...

# Download golangci-lint if necessary
golangci-lint:
ifneq "$(GOLANGCI_LINT_CURRENT_VERSION)" "$(GOLANGCI_LINT_REQUIRED_VERSION)"
	@cd $(GOPATH)
	@curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(GOLANGCI_LINT_INSTALL_PATH) v$(GOLANGCI_LINT_REQUIRED_VERSION)
	
GOLANGCI_LINT = $(GOLANGCI_LINT_INSTALL_PATH)/golangci-lint
else
GOLANGCI_LINT = $(shell which golangci-lint)
endif